<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Sobre extends Model
{
    protected $table = 'sobre';

    protected $guarded = ['id'];

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 1200,
            'height' => null,
            'path'   => 'assets/img/sobre/'
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 550,
            'height' => null,
            'path'   => 'assets/img/sobre/'
        ]);
    }
}

<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SobreRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem_1' => 'image',
            'texto' => 'required',
            'imagem_2' => 'image',
        ];
    }
}

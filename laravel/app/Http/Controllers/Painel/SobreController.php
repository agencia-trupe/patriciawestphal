<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SobreRequest;
use App\Http\Controllers\Controller;

use App\Models\Sobre;

class SobreController extends Controller
{
    public function index()
    {
        $registro = Sobre::first();

        return view('painel.sobre.edit', compact('registro'));
    }

    public function update(SobreRequest $request, Sobre $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem_1'])) $input['imagem_1'] = Sobre::upload_imagem_1();
            if (isset($input['imagem_2'])) $input['imagem_2'] = Sobre::upload_imagem_2();

            $registro->update($input);

            return redirect()->route('painel.sobre.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}

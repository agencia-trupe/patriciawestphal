<?php

namespace App\Http\Controllers;

use App\Models\Sobre;

class HomeController extends Controller
{
    public function index()
    {
        $sobre = Sobre::first();

        return view('frontend.home', compact('sobre'));
    }
}

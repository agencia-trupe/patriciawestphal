<?php

namespace App\Http\Controllers;

use App\Models\Projeto;

class PortfolioController extends Controller
{
    public function index()
    {
        $projetos = Projeto::with('imagens')->ordenados()->get();

        return view('frontend.portfolio', compact('projetos'));
    }
}

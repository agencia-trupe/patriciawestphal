@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="center">
            <img src="{{ asset('assets/img/sobre/'.$sobre->imagem_1) }}" alt="">

            <div class="texto">
                {!! $sobre->texto !!}
            </div>

            <img src="{{ asset('assets/img/sobre/'.$sobre->imagem_2) }}" alt="">
        </div>
    </div>

@endsection

@extends('frontend.common.template')

@section('content')

    <div class="contato">
        <div class="center">
            <form action="{{ route('contato.post') }}" method="POST">
                {!! csrf_field() !!}

                <p class="telefone">{{ $contato->telefone }}</p>
                <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>

                @if(session('enviado'))
                <div class="flash">Mensagem enviada com sucesso!</div>
                @endif
                @if($errors->any())
                <div class="flash">Preencha todos os campos corretamente.</div>
                @endif

                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                <div class="wrapper">
                    <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                    <input type="submit" value="enviar">
                </div>
            </form>

            <div class="informacoes">
                <p class="endereco">{!! $contato->endereco !!}</p>
                <div class="mapa">{!! $contato->google_maps !!}</div>
            </div>
        </div>
    </div>

@endsection

    <footer>
        <div class="center">
            <div class="wrapper">
                <nav>
                    <a href="{{ route('home') }}">HOME</a>
                    <a href="{{ route('portfolio') }}">PORTFOLIO</a>
                    <a href="{{ route('contato') }}">CONTATO</a>
                </nav>

                <p class="telefone">{{ $contato->telefone }}</p>

                <div class="social">
                    @foreach(['instagram', 'facebook', 'pinterest'] as $s)
                        @if($contato->{$s})
                        <a href="{{ Tools::parseLink($contato->{$s}) }}" class="{{ $s }}" target="_blank">
                            {{ $s }}
                        </a>
                        @endif
                    @endforeach
                </div>
            </div>

            <div class="copyright">
                <p>
                    © {{ date('Y') }} {{ config('app.name') }} | Todos os direitos reservados
                    <span>|</span>
                    <a href="https://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="https://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>

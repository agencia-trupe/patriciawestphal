@extends('frontend.common.template')

@section('content')

    <div class="portfolio">
        <div class="center">
            @foreach($projetos as $projeto)
            <a href="#" class="projeto" data-projeto-id="{{ $projeto->id }}">
                <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="">
                <div class="titulo">
                    <span></span>
                    <p>{{ $projeto->titulo }}</p>
                </div>
            </a>
            @endforeach
        </div>
    </div>

    <div style="display:none">
        @foreach($projetos as $projeto)
            @foreach($projeto->imagens as $imagem)
            <a class="fancybox" href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" rel="galeria-{{ $projeto->id }}"></a>
            @endforeach
        @endforeach
    </div>

@endsection

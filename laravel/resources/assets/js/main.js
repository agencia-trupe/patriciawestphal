import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('.fancybox').fancybox({
    padding: 0,
    closeBtn: false,
    margin: [20, 64, 20, 64],
    maxWidth: 1100,
});

$('.projeto').click(function handler(event) {
    event.preventDefault();

    const id = $(this).data('projeto-id');

    if (id) {
        $(`a[rel=galeria-${id}]`)[0].click();
    }
});
